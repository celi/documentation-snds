# DIM - Département d'information médicale
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le Département d'Information Médicale (DIM) d'un hôpital gère l'information de santé des patients.

Il est notamment en charge du <link-previewer href="codage.html" text="codage" preview-title="Codage" preview-text="Le codage de l'information médicale est l'activité consistant à transformer des données médicales textuelles en des codes tirés de classifications, telles que la CCAM ou la CIM. " /> de l'activité médicale dans un but de remboursement des prestations hospitalières par l'Assurance-maladie.


## Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/D%C3%A9partement_d%27information_m%C3%A9dicale)
- Article de blog [Le DIM pour "les nuls"](https://www.departement-information-medicale.com/blog/2010/03/28/le-dim-pour-les-nuls/)
    