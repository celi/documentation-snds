# Case-mix
<!-- SPDX-License-Identifier: MPL-2.0 -->

Anglicisme désignant l’éventail des cas médicaux et chirurgicaux traités par un établissement de santé et qui peuvent être décrits :
- par groupes homogènes de malades (<link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " />), 
- par groupes homogènes de journées (<link-previewer href="GHJ.html" text="GHJ" preview-title="GHJ - Groupe homogène de journées" preview-text="Le groupe homogène de journées est la catégorie élémentaire de la classification médico-économique propre au PMSI en SSR. Il se fonde sur les informations médico-administratives figurant dans le résumé hebdomadaire standardisé (RHS réalisé par les établissements pour chaque patient." />) pour les prises en charge en soins de suite ou de réadaptation, 
- par groupes homogènes de tarifs (<link-previewer href="GHT.html" text="GHT" preview-title="GHT - Groupe homogène de tarifs" preview-text="Le groupe homogène de tarifs correspond aux tarifs journaliers applicables en hospitalisation à domicile (HAD. " />) pour les soins en hospitalisation à domicile, 
- par catégorie majeure de diagnostic (<link-previewer href="CMD.html" text="CMD" preview-title="CMD - Catégorie majeure de diagnostic" preview-text="La classification médico-économique utilisée pour le PMSI en MCO est structurée en 28 catégories majeures de diagnostic (CMD. " />), 
- par catégorie majeure clinique (<link-previewer href="CMC.html" text="CMC" preview-title="CMC - Catégories majeures cliniques" preview-text="Les Catégories majeures cliniques correspondent aux grands types de prise en charge des services de soins de suite et de réadaptation (SSR : cardiovasculaire et respiratoire, neuromusculaire, post-traumatique, soins palliatifs, etc." />). 

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/case-mix) sur le site internet du ministère
