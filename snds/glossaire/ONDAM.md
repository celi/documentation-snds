# ONDAM - Objectif national des dépenses d’assurance maladie
<!-- SPDX-License-Identifier: MPL-2.0 -->

L’ONDAM est fixé chaque année par le Parlement, conformément aux dispositions de la loi de financement de la sécurité sociale (<link-previewer href="LFSS.html" text="LFSS" preview-title="LFSS - Loi de financement de la Sécurité sociale" preview-text="La loi de financement de la sécurité sociale (LFSS) est votée chaque année par le Parlement ; elle fixe l’objectif national des dépenses d’assurance maladie (Ondam." />). 

Il correspond aux prévisions de recettes et aux objectifs des dépenses de la sécurité sociale : 
- les soins de ville, 
- les établissements de santé publics et privés,
- les établissements médico-sociaux,
- les autres prises en charge.
 
Au sein des établissements de santé, des sous objectifs sont fixés pour : 
- les établissements publics et privés tarifés à l’activité au titre 
  - de leur activité de médecine, chirurgie, obstétrique (ODMCO : objectif des dépenses de <link-previewer href="MCO.html" text="MCO" preview-title="MCO - Médecine, chirurgie, obstétrique" preview-text="Terme utilisé pour désigner les activités aigus de courte durée réalisées dans les établissements de santé, en hospitalisation (avec ou sans hébergement) ou en consultations externes." />), 
  - de leurs missions d’intérêt général et d’aide à la contractualisation (<link-previewer href="MIGAC.html" text="MIGAC" preview-title="MIGAC - Missions d’intérêt général et de l’aide à la contractualisation " preview-text="La dotation de financement des MIGAC permet de financer les activités des établissements de MCO qui ne sont pas tarifées à l’activité. " />)
- les activités de soins de suite et de réadaptation, de psychiatrie, et de soins de longue durée : 
  - l’objectif des dépenses d’assurance maladie (<link-previewer href="ONDAM.html" text="ODAM" preview-title="ONDAM - Objectif national des dépenses d’assurance maladie" preview-text="L’ONDAM est fixé chaque année par le Parlement, conformément aux dispositions de la loi de financement de la sécurité sociale (LFSS. " />) 
  - l’objectif quantifié national (<link-previewer href="OQN.html" text="OQN" preview-title="OQN - Objectif quantifié national" preview-text="cf ONDAM" />)

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/ONDAM)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/ondam-objectif-national-des-depenses-d-assurance-maladie) sur le site internet du ministère
