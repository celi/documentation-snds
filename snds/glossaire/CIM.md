# CIM - Classification internationale des maladies
<!-- SPDX-License-Identifier: MPL-2.0 -->

La classification internationale des maladies, publiée par l’Organisation mondiale de la santé (<link-previewer href="OMS.html" text="OMS" preview-title="OMS - Organisation mondiale de la santé" preview-text="Aucune définition détaillée n'existe pour l'instant dans le glossaire. Pour contribuer, référrez-vous au guide de contribution." />) est utilisée pour coder les diagnostics dans les recueils d’information des différents domaines du <link-previewer href="PMSI.html" text="PMSI" preview-title="PMSI - Programme de médicalisation des systèmes d’information" preview-text="Le PMSI permet de décrire de façon synthétique et standardisée l’activité médicale des établissements de santé. Il repose sur l’enregistrement de données médico-administratives normalisées dans un recueil standard d’information. Il comporte 4 « champs » : « médecine, chirurgie, obstétrique et odontologie » (MCO) « soins de suite ou de réadaptation » (SSR) « psychiatrie » sous la forme du RIM-Psy (recueil d’information médicale en psychiatrie) « hospitalisation à domicile » (HAD)" />. 

La version utilisée en France depuis 1996 est la dixième révision (CIM-10).

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Classification_internationale_des_maladies)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/classification-internationale-des-maladies-cim) sur le site internet du ministère
