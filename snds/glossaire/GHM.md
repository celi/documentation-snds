# GHM - Groupe homogène de malades
<!-- SPDX-License-Identifier: MPL-2.0 -->

Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en <link-previewer href="MCO.html" text="MCO" preview-title="MCO - Médecine, chirurgie, obstétrique" preview-text="Terme utilisé pour désigner les activités aigus de courte durée réalisées dans les établissements de santé, en hospitalisation (avec ou sans hébergement) ou en consultations externes." />. 
Chaque séjour aboutit dans un GHM selon un algorithme fondé sur les informations médico-administratives contenues dans le résumé de sortie standardisé (<link-previewer href="RSS.html" text="RSS" preview-title="RSS - Résumé de sortie standardisé" preview-text="Le résumé de sortie standardé (RSS) est constitué de l'ensemble des RUM relatifs au même séjour hospitalier d'un malade dans le secteur MCO." />) de chaque patient.

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/groupe-homogene-de-malades-ghm) sur le site internet du ministère
